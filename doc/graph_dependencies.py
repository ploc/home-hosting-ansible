#!/usr/bin/env python3
# coding=utf-8
#
# this script generate a dependency graph of roles used in playbooks
#
# it has been copied from Stack Overflow
# http://stackoverflow.com/questions/21721942/is-there-an-easy-way-to-generate-a-graph-of-ansible-role-dependencies#34077658
#
# this script is licenced under cc by-sa 3.0 with attribution required
# https://creativecommons.org/licenses/by-sa/3.0/
#
# authors are Ikar Pohorský and sebn
# http://stackoverflow.com/users/955422/ikar-pohorsk%c3%bd
# http://stackoverflow.com/users/5637179/sebn
#
# the following commands might be required to fulfill its prerequisites
# sudo apt-get install python3-gv

import sys
import gv
from glob import glob
import yaml

g = gv.digraph('roles')

role_nodes = {}

def add_role(role):
    if role not in role_nodes:
        role_nodes[role] = gv.node(g, role)

def link_roles(dependent, depended):
    gv.edge(
        role_nodes[dependent_role],
        role_nodes[depended_role]
    )

for path in glob('../playbooks/roles/*/meta/main.yml'):
    dependent_role = path.split('/')[3]

    add_role(dependent_role)

    with open(path, 'r') as f:
        for dependency in yaml.load(f.read(), Loader=yaml.SafeLoader)['dependencies']:
            depended_role = dependency['role']

            add_role(depended_role)
            link_roles(dependent_role, depended_role)

gv.layout(g, 'dot')
gv.write(g, 'roles_dependencies_autogen.dot')
gv.render(g, 'png', 'roles_dependencies_autogen.png')

print("dot file is available: roles_dependencies_autogen.dot")
print("png file is available: roles_dependencies_autogen.png")
