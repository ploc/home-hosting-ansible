#!/bin/bash
#
# this script generate a graph from a dot file
#
# this script is licenced under cc by-sa 3.0 with attribution required
# https://creativecommons.org/licenses/by-sa/3.0/
#
# author is Ploc
#
# the following commands might be required to fulfill its prerequisites
# sudo apt-get install graphviz

inputFilename="roles_dependencies.dot"
outputFilename="roles_dependencies.png"

dot -Tpng -o ${outputFilename} ${inputFilename}

echo "png file is available: roles_dependencies.png"
