<?php
$dir = "reader/books/";
$files = array_slice(scandir($dir), 2);

$content = "";
foreach ($files as $book) {
 $content .= '<li class="files"><a href="reader/?bookPath=books/' . $book . '" title="' . $book . '" class="files"><span class="icon file f-epub">.epub</span><span class="name">' . $book . '</span> <span class="details">ebook</span></a></li>';
}

$header = <<<EOT
<!doctype html>
<html lang="fr" style="background-attachment: scroll;">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <title>epub browser</title>
    <link href="app.css" rel="stylesheet" />
  </head>
  <body>
    <div class="filemanager">
      <div class="breadcrumbs"><span class="folderName">book</span> <span class="arrow">→</span> <span class="folderName">chooser</span></div>
      <ul class="data animated">
EOT;

$footer = <<<EOT
      </ul>
    </div>
    <footer>
      <span>select a book you want to read</span>
    </footer>
  </body>
</html>
EOT;

print $header;
print $content;
print $footer;

?>
