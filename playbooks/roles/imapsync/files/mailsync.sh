#!/bin/bash
#
# synchronize mails between main and backup mail accounts

# default config
shellName="$(basename "${0}")"
shellNiceName="$(basename "${0%.*}")"
shellDirName=$(dirname "${0}")

source "${shellDirName}/conf/mailsync.conf"
source "${shellDirName}/conf/mailsync.server1.conf"
source "${shellDirName}/conf/mailsync.server2.conf"

# functions

###
 # display usage
 ##
displayUsage () {
    cat <<EOF
usage - ${shellName} [-c] [-ll logLevel] [-lf logFile] [-t]
  -c  - enable check imap folders mode
  -ll - set the log level
  -lf - set the log file
  -t  - enable test mode
EOF
}

###
 # log a message
 #
 # @global $logLevel the log level
 # @global $logFile the log file
 # @param $1 the log level of the message
 # @param $2 the log message
 ##
log () {
    local paramLogLevel="${1}"
    local paramLogMessage="${2}"

    local logDate="$(date +%H:%M:%S)"
    local logMessage="[${logDate}] ${paramLogMessage}"

    if [ "${paramLogLevel}" -le "${logLevel}" ]
    then
        echo "${logMessage}"
    fi

    if [ ! -z "${logFile}" ]
    then
        echo "${logMessage}" >> "${logFile}"
    fi
}

# getting parameters value
while [ "$#" -ge 1 ] ; do
    case "$1" in
        -c) # enable check imap folders mode
            shiftStep=1
            checkImapFoldersMode="yes"
            ;;
        -ll) # set the log level
            shiftStep=2
            logLevel="${2}"
            ;;
        -lf) # set the log file
            shiftStep=2
            logFile="${2}"
            ;;
        -t) # enable test mode
            shiftStep=1
            testMode="yes"
            ;;
        *)
            echo "ERROR - invalid arguments, exiting"
            displayUsage
            exit 2
            ;;
    esac

    if [ "$#" -ge "${shiftStep}" ]
    then
        shift "${shiftStep}"
    else
        echo "ERROR - invalid number of arguments, exiting"
        displayUsage
        exit 3
    fi
done

# setting parameters value
if [ -z "${checkImapFoldersMode}" ]
then
    checkImapFoldersMode="${defaultCheckImapFoldersMode}"
fi

if [ -z "${logLevel}" ]
then
    logLevel="${defaultLogLevel}"
fi

if [ -z "${logFile}" ]
then
    logFile="${defaultLogFile}"
fi

if [ -z "${testMode}" ]
then
    testMode="${defaultTestMode}"
fi

# script
imapsyncCommand="${shellDirName}/imapsync --nolog"

if [ "${testMode}" = "yes" ]
then
    imapsyncCommand="${imapsyncCommand} --dry"
fi

log 1 "${shellNiceName} started"

if [ "${testMode}" = "yes" ]
then
    log 1 "WARNING - test mode enabled"
fi
log 2 "imapsync command is [${imapsyncCommand}]"

if [ "${checkImapFoldersMode}" = "yes" ]
then
    log 1 "check if some folders on the source account do not (yet ?) exist on the destination account"
    log 1 "WARNING - output won't be logged !"
    # they should be created during the next synchronization
    ${imapsyncCommand} \
        --host1 "${host1}" --user1 "${user1}" --passfile1 "${passfile1}" "${host1AdditionalParametersAsOrigin}" \
        --host2 "${host2}" --user2 "${user2}" --passfile2 "${passfile2}" "${host2AdditionalParametersAsDestination}" \
        --justfoldersizes

    # checking imapsync return code
    if [ "$?" -ne 0 ]
    then
        log 1 "ERROR - an error occured while executing imapsync command, exiting"
        exit 1
    fi

    log 1 "check if some folders on destination account do not exist (any more ?) on the source account"
    log 1 "WARNING - output won't be logged !"
    # as a security feature, imapsync does not delete folders on destination account
    # these folders will have to be manually deleted
    ${imapsyncCommand} \
        --host1 "${host2}" --user1 "${user2}" --passfile1 "${passfile2}" "${host2AdditionalParametersAsOrigin}" \
        --host2 "${host1}" --user2 "${user1}" --passfile2 "${passfile1}" "${host1AdditionalParametersAsDestination}" \
        --justfoldersizes

    # checking imapsync return code
    if [ "$?" -ne 0 ]
    then
        log 1 "ERROR - an error occured while executing imapsync command, exiting"
        exit 1
    fi
else
    log 1 "synchronizing (output will be logged in log file ${logFile})"
    # real synchronization (except on test mode)
    ${imapsyncCommand} \
        --host1 "${host1}" --user1 "${user1}" --passfile1 "${passfile1}" "${host1AdditionalParametersAsOrigin}" \
        --host2 "${host2}" --user2 "${user2}" --passfile2 "${passfile2}" "${host2AdditionalParametersAsDestination}" \
        --exclude "${excludeFolders1}" \
        --useheader "Message-ID" --skipsize \
        --subscribe \
        --expunge1 --expunge2 \
        --delete2 \
        --syncinternaldates \
        >> ${logFile}

    # checking mailsync return code
    if [ "$?" -ne 0 ]
    then
        log 1 "ERROR - an error occured while executing mailsync command, exiting"
        exit 1
    fi
fi

log 1 "${shellNiceName} finished"
