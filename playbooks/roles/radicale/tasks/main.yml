---
- block:

  - name: install package and dependency packages
    apt:
      name: ["radicale", "libapache2-mod-wsgi-py3", "python3-passlib"]
      state: present
      cache_valid_time: 3600

  - name: enable mod_wsgi and mod_headers
    apache2_module:
      name: "{{ item }}"
      state: present
    with_items:
      - headers
      - wsgi
    notify: restart apache2

  # TODO this task is a workaround
  # this task is reusing an existing line in the file so that the new line is inserted only once
  # see https://modwsgi.readthedocs.io/en/master/configuration-directives/WSGISocketPrefix.html
  - name: configure WSGISocketPrefix
    lineinfile:
      state: present
      dest: /etc/apache2/apache2.conf
      regexp: see README.Debian
      line: WSGISocketPrefix /var/run/apache2
      backrefs: yes
    notify: restart apache2

  - name: add ops_user to radicale group
    user:
      name: "{{ _radicale.ops_user }}"
      groups: radicale
      append: yes

  - name: reset ssh connection to make group membership effective
    meta: reset_connection

  - name: create directory for radicale web space
    file:
      path: /var/www/{{ _radicale.apache_segment }}
      state: directory

  - name: create home directory
    file:
      path: /var/lib/radicale
      owner: radicale
      group: radicale
      mode: u=rwx,g=rx,o=
      state: directory

  - name: create symlinks for web space
    file:
      src: /var/www/{{ _radicale.apache_segment }}
      dest: "{{ _radicale.apache_path }}"
      state: link

  - name: create symlinks for radicale.wsgi configuration file
    file:
      src: /usr/share/radicale/radicale.wsgi
      dest: /var/www/{{ _radicale.apache_segment }}/radicale.wsgi
      state: link

  - name: copy radicale.conf configuration file
    template:
      src: radicale.conf.j2
      dest: /etc/apache2/{{ _radicale.apache_services_available_segment }}/radicale.conf
    notify: restart apache2

  - name: create symlinks for radicale apache conf
    file:
      src: ../{{ _radicale.apache_services_available_segment }}/radicale.conf
      dest: /etc/apache2/{{ _radicale.apache_services_enabled_segment }}/radicale.conf
      state: link
    notify: restart apache2

  - name: create directory for apps datastore
    file:
      path: "{{ _radicale.datastore_apps_path }}"
      owner: "{{ _radicale.ops_user }}"
      group: "{{ _radicale.ops_group }}"
      mode: "{{ _radicale.datastore_apps_mode }}"
      state: directory

  - name: create directory for radicale data
    file:
      path: "{{ _radicale.data_path }}"
      owner: radicale
      group: radicale
      mode: u=rwx,g=rx,o=
      state: directory

  - name: create symlinks in mount point
    file:
      src: "{{ _radicale.data_path }}"
      dest: /var/lib/radicale/collections
      state: link

  - name: generate radicale users configuration file
    htpasswd:
      path: /etc/radicale/users
      name: "{{ item.login }}"
      password: "{{ item.password }}"
      owner: root
      group: www-data
      mode: u=rw,g=r,o=
    with_items: "{{ _radicale.users }}"

  - name: copy radicale rights configuration file
    template:
      src: rights.j2
      dest: /etc/radicale/rights

  - name: check that apache config is valid
    command: apache2ctl configtest
    changed_when: false

  become: yes
