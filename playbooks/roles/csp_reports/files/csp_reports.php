<?php 
/*
 * this script requires php >= 5.4
 * this script is inspired from https://mathiasbynens.be/notes/csp-reports
 **/

// specify the path file for violation reports
define("FILE", "data/reports.txt");

// send `204 No Content` status code
http_response_code(204);

// get the raw POST data
$data = file_get_contents("php://input");

// only continue if it's valid JSON that is not just `null`, `0`, `false` or an
// empty string, i.e. if it could be a CSP violation report
if ($data = json_decode($data, true)) {
  // get source-file to perform some tests
  $blocked_uri = $data["csp-report"]["blocked-uri"];
  if (isset($data["csp-report"]["source-file"])) {
    $source_file = $data["csp-report"]["source-file"];
  } else {
    $source_file = null;
  }

  if (// avoid false positives notifications coming from Chrome extensions (Wappalyzer, MuteTab, etc.)
      // see https://code.google.com/p/chromium/issues/detail?id=524356
      strpos($source_file, "chrome-extension://") === false

      // avoid false positives notifications coming from Safari extensions (diigo, evernote, etc.)
      && strpos($source_file, "safari-extension://") === false
      && strpos($blocked_uri, "safari-extension://") === false

      // search engine extensions?
      && strpos($source_file, "se-extension://") === false

      // added by browsers in webviews
      && strpos($blocked_uri, "webviewprogressproxy://") === false

      // Google search app
      // see https://github.com/nico3333fr/CSP-useful/commit/ecc8f9b0b379ae643bc754d2db33c8b47e185fd1
      && strpos($blocked_uri, "gsa://onpageload") === false
    ) {
    // prettify the JSON-formatted data
    $data = json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
    $data = date("c") . " " . $data . "\n";

    // simply store the CSP violation report
    file_put_contents (FILE, $data, FILE_APPEND);
  }
}
?>
