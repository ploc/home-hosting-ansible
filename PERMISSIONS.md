# apps

## apaxy

- path: `~/apps/apaxy-2.3.0/files/`

- needs
  - ops needs to read and write
  - www-data needs to read

- settings
  - add user ops to group www-data
  - owner ops, group www-data
  - chmod 750 for dirs
  - chmod 640 for files

## csp_report

- path: `~/apps/csp_reports/data/`

- needs
  - www-data needs to read and write
  - ops needs to read and delete

- settings
  - add user ops to group www-data
  - owner www-data, group www-data
  - chmod 770 for dirs (770 is for allowing deleting to group)
  - chmod 640 for files

## freshrss

- path: `/media/external/apps-data/freshrss/`

- needs
  - ops needs to read
  - www-data needs to read and write

- settings
  - add user ops to group www-data
  - owner www-data, group www-data
  - chmod 750 for dirs
  - chmod 640 for files

## epub.js

- path: `~/apps/epubjs-reader-master/reader/books/`

- needs
  - ops needs to read and write
  - www-data needs to read

- settings
  - add user ops to group www-data
  - owner ops, group www-data
  - chmod 750 for dirs
  - chmod 640 for files

## mpd

- path: `/media/external/apps-data/mpd/`

- needs
  - ops needs to read and write music
  - mpd needs to read music
  - mpd needs to read and write playlist

- settings
  - add user ops to group audio
  - /media/external/apps-data/mpd : owner ops, group audio
  - /media/external/apps-data/mpd/music : owner ops, group audio
  - /media/external/apps-data/mpd/playlists : owner mpd, group audio
  - chmod 750 for dirs
  - chmod 640 for files

## phpmyaddressbook

- path: `/media/external/apps-data/phpmyaddressbook/`

- needs
  - ops needs to read
  - www-data needs to read and write

- settings
  - add user ops to group www-data
  - owner www-data, group www-data
  - chmod 750 for dirs
  - chmod 640 for files

## radicale

- path: `/media/external/apps-data/radicale/`

- needs
  - ops needs to read
  - radicale needs to read and write

- settings
  - add user ops to group radicale
  - owner radicale, group radicale
  - chmod 750 for dirs
  - chmod 640 for files

## transmission

- path: `/media/external/apps-data/transmission/`

- needs
  - ops needs to read and delete
  - transmission needs to read and write

- settings
  - add user ops to group debian-transmission
  - owner debian-transmission, group debian-transmission
  - chmod 770 for dirs (770 is for allowing deleting to group)
  - chmod 640 for files

## wallabag

- path: `/media/iomega/apps-data/wallabag/data/`
- needs
  - ops needs to read
  - www-data needs to read and write
- settings
  - add user ops to group www-data
  - owner www-data, group www-data
  - chmod 750 for dirs
  - chmod 640 for files

# dev

## git

- path: `/media/external/dev-data/git/`

- needs
  - ops needs to read and write

- settings
  - owner ops, group ops
  - chmod 700 for dirs
  - chmod 600 for files

## web

- path: `/media/external/dev-data/web/`

- needs
  - ops needs to read and write
  - www-data needs to read (and sometime to write)

- settings
  - add user ops to group www-data
  - owner ops, group www-data
  - setgid www-data for automatic group setting
  - chmod 750 for dirs (with 770 exceptions when http server needs write permission)
  - chmod 640 for files (with 660 exceptions when http server needs write permission)
